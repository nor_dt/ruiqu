<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <title>Laravel&Vue</title>
</head>
<body style="background-color: #efefef">
<div id="app" class="container-full">
    <router-view class="view"></router-view>
    <navigation-bar v-show="isShow"></navigation-bar>

</div>
</body>
<script type="text/javascript">
</script>
<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/jquery-scrolltofixed-min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#footer').scrollToFixed( {
            bottom: 0
        });
    })
</script>
</html>