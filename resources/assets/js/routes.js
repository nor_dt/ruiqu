export default[
    { path: '', redirect: '/recommend' },

    { path: '/navigationBar', component: require('./page/public/navigationBar.vue') },
    { path: '/my', component: require('./page/app/my.vue') },
    { path: '/recommend', component: require('./page/app/recommend.vue') },
    { path: '/channel', component: require('./page/app/channel.vue') },
    { path: '/login', component: require('./page/app/login.vue') },
    { path: '/register', component: require('./page/app/register.vue') },
    { path: '/find', component: require('./page/app/find.vue') }
    ];
