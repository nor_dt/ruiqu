import Vue from 'vue';

import VueResource from 'vue-resource';
Vue.use(VueResource);
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import store from './store/'; // vuex 数据存储所需对象
import routes from './routes';    // 路由配置文件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
Vue.use(ElementUI);


// 实例化路由
const router = new VueRouter({
    routes
});

Vue.component(
    'navigation-bar',
    require('./page/public/navigationBar.vue')
);

// 实例化 Vue
var vm = new Vue({
    data:{
        isShow :1,
    },
    store,
    router
}).$mount('#app');

