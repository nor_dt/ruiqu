<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobile');//手机号
            $table->string('nickname');//昵称
            $table->string('level');//等级
            $table->string('name');//姓名
            $table->string('wechat');//微信号
            $table->string('qq');//qq号
            $table->string('email');//邮箱
            $table->string('desc');//描述
            $table->string('password');//密码
            $table->string('status');//用户状态
            $table->string('credit');//信用分数
            $table->string('over');//余额
            $table->string('certification');//认证,分割
            $table->string('sex')->default(1);//性别
            $table->string('home');//家乡
            $table->string('Uid');//uid
            $table->string('avatar')->default('http://oqc9f0eyy.bkt.clouddn.com/default.png');//头像
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
